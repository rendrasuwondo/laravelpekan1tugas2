<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//TUGAS PEKAN1 HARI 2
Route::middleware(['auth', 'route'])->group(function () {
    Route::get('/route-1', 'RouteController@route_1');
    Route::get('/route-2', 'RouteController@route_2');
    Route::get('/route-3', 'RouteController@route_3');
});
//TUGAS PEKAN1 HARI 2 END


Route::get('/', function () {
    return view('welcome');
});

// Route::get('/test', 'TestController@test')->middleware('dateMiddleware');
// Route::get('/test', 'TestController@test');

Route::middleware('dateMiddleware')->group(function () {
    Route::get('/test', 'TestController@test');
    Route::get('/test1', 'TestController@test1');
});

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('admin', 'TestController@admin');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
