<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RouteController extends Controller
{
    public function route_1()
    {
        return 'berhasil masuk Route 1';
    }

    public function route_2()
    {
        return 'berhasil masuk Route 2';
    }

    public function route_3()
    {
        return 'berhasil masuk Route 3';
    }
}
