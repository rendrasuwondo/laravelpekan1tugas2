<?php

namespace App\Http\Controllers;

use App\Http\Middleware\DateMiddleware;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class TestController extends Controller
{

    public function __construct()
    {
        // $this->middleware('dateMiddleware');
    }

    public function test()
    {

        return 'berhasil masuk';
    }

    public function test1()
    {

        return 'berhasil masuk test1';
    }

    public function admin()
    {
        // dd(Auth::user());
        return 'Admin berhasil masuk';
    }
}
