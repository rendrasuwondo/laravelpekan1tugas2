<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RouteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Super Admin
        if (Auth::user()->role_id == 2) {
            return $next($request);
        }

        //Admin
        if (Auth::user()->role_id == 1 && $request->segment(1) != "route-1") {
            return $next($request);
        }

        //Guest
        if (Auth::user()->role_id == 0 && $request->segment(1) == "route-3") {
            return $next($request);
        }

        abort(403);
    }
}
